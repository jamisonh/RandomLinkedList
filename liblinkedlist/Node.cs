﻿using System;
using System.Collections.Generic;
namespace RandomLinkedList
{

    public class Node
    {
        private string _tag;
        /// <summary>
        /// This Nodes unique object identifier - Used for proving separate objects
        /// </summary>
        public string ObjId = Guid.NewGuid().ToString();

        /// <summary>
        /// The next node in the linked list
        /// </summary>
        public Node Next { get; set; }

        /// <summary>
        /// The random reference to another node in the linked list.
        /// </summary>
        public Node Reference { get; set; }


        /// <summary>
        /// Returns this nodes tag, represents the data inside the node.
        /// </summary>
        /// <returns></returns>
        ///
        public string Tag => _tag;


        /// <summary>
        /// Creates a new Node, with the provided "data" where the "data" is represented by the node Tag.
        /// </summary>
        /// <param name="nodeTag"></param>
        public Node(string tag)
        {
            _tag = tag;
        }

        /// <summary>
        /// "Deep" copies the provided Node, such that none of the nodes in the clone have any dependency on the original.
        /// </summary>
        /// <param name="original">The original linked list head node to clone</param>
        /// <returns>The fully cloned Linked list</returns>
        public static Node DuplicateList(Node original)
        {
            var copy = CopyLinkedList(original);
            var originalLinks = ShiftOriginalRefs(original, copy);
            UpdateClonedReferences(copy);
            RestoreOriginalLinks(original, originalLinks);

            return copy;
        }

        /// <summary>
        /// Repoints every nodes next value in the original linked list and moves its pointer to the copy, at the same position.
        /// During this, the copies reference is also repointed to the originals reference.  
        /// </summary>
        /// <param name="original"></param>
        /// <param name="copy"></param>
        /// <returns>The original lists *original* next values, positionally aligned in the IEnumberable where they appeared in the linked list.</returns>
        private static IEnumerable<Node> ShiftOriginalRefs(Node original, Node copy)
        {
            var originalLinks = new List<Node>();
            while (original.Next != null)
            {
                var tmp = original.Next;
                originalLinks.Add(tmp);

                original.Next = copy;
                copy.Reference = original.Reference;
                original = tmp;
                copy = copy.Next;
            }
            original.Next = copy;
            copy.Reference = original.Reference;
            return originalLinks;
        }

        /// <summary>
        /// Uses the input list of nodes to positionally replace the original linked's list .Next reference back to the original "next" reference
        /// </summary>
        /// <param name="original"></param>
        /// <param name="originalLinks"></param>
        private static void RestoreOriginalLinks(Node original, IEnumerable<Node> originalLinks)
        {
            foreach (var node in originalLinks)
            {
                original.Next = node;
                original = original.Next;

            }
            original.Next = null;
        }

        /// <summary>
        /// Updates the random reference pointer for each node in the copy from it's temporary 
        /// reference to an original linked lists reference, to the final reference within the copy.
        /// </summary>
        /// <param name="copy">The copy to </param>
        private static void UpdateClonedReferences(Node copy)
        {
            while (copy.Next != null)
            {
                copy.Reference = copy.Reference.Next;
                copy = copy.Next;
            }
            copy.Reference = copy.Reference.Next;
        }


        /// <summary>
        /// Creates a copy of the nodes, next pointers and data of a linked list but ignores the reference.
        /// </summary>
        /// <param name="original">the head node to start the copy from</param>
        /// <returns>a copy of the input node</returns>
        private static Node CopyLinkedList(Node original)
        {
            var copy = new Node(original.Tag);
            //Preserving copy before we loop through and add additional copies
            var tmp = copy;
            while (original.Next != null)
            {
                tmp.Next = new Node(original.Next.Tag);
                original = original.Next;
                tmp = tmp.Next;
            }
            return copy;
        }
    }
}
