﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RandomLinkedList
{
    public static class NodeListGenerator
    {
        public const int MaxNodes = 100000000;
        private static readonly Random Rand = new Random();

        /// <summary>
        /// Creates a linked list of nodes at the desired length, populates the Tags, object identifiers, and random references to other items in the list.
        /// </summary>
        /// <param name="listLength"></param>
        public static Node Generate(int listLength)
        {
            if (listLength > MaxNodes)
            {
                throw new ArgumentOutOfRangeException($"Cmon...{MaxNodes} isn't enough for you?");
            }
            
            var nodes = new Node[listLength];
            var nodeTag = 1;
            var first = new Node(RandomString(8));

            var curr = first;
            nodes[0] = curr;
            for (var i = 1; i < listLength; i++)
            {
                curr.Next = new Node(RandomString(8));
                curr = curr.Next;
                nodes[i] = curr;
            }

            RandomizeReferences(nodes);

            return first;
        }

        private static void RandomizeReferences(IReadOnlyList<Node> nodes)
        {
            var i = 0;
            foreach (var node in nodes)
            {
                node.Reference = nodes[GetPositionalRefNotSelf(i, nodes.Count)];
                i++;
            }
        }

        private static int GetPositionalRefNotSelf( int curr, int size)
        {
            int positionalRef;
            while (true)
            {
                positionalRef = Rand.Next(0, size);
                if (positionalRef != curr)
                {
                    break;
                }
            }

            return positionalRef;
        }


        //Blatantly borrowed from https://stackoverflow.com/questions/1344221/how-can-i-generate-random-alphanumeric-strings-in-c
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[Rand.Next(s.Length)]).ToArray());
        }
    }
}
