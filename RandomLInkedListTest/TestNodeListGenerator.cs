﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RandomLinkedList;
using System.Diagnostics;

namespace RandomLInkedListTest
{
    [TestClass]
    public class TestNodeListGenerator
    {
        [TestMethod]
        public void TestGeneratorLength()
        {
            var expectedNodes = 10;
            var node = NodeListGenerator.Generate(expectedNodes);
            var nodeCount = 1;
            while (node.Next != null) {
                nodeCount++;
                node = node.Next;
            }
            Assert.AreEqual(expectedNodes, nodeCount);
        }


        [TestMethod]
        public void TestAllRefs()
        {
            var node = NodeListGenerator.Generate(10000);
            while (node.Next != null)
            {
                Assert.IsNotNull(node.Reference);
                Assert.AreNotSame(node, node.Next);
                node = node.Next;
            }
            //check the last node
            Assert.IsNotNull(node.Reference);
            Assert.IsNull(node.Next);
        }

        [TestMethod]
        public void TestRefsNotSelf()
        {
            //create a 100 2 tree nodes and verify the randomized ref is never self referencing (IE 2 -> 1 and 1 -> 2)
            for (var i = 0; i < 100; i++)
            {
                var node = NodeListGenerator.Generate(2);
                Assert.AreNotSame(node, node.Reference);
                Assert.AreNotSame(node.Next, node.Next.Reference);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestMaxInt()
        {
            NodeListGenerator.Generate(NodeListGenerator.MaxNodes+1);
        }
    
        //[TestMethod]
        //public void TestTags()
        //{
        //    var node = NodeListGenerator.Generate(2);
        //    Assert.AreEqual("1", node.Tag);
        //    Assert.AreEqual("2", node.Next.Tag);
        //    Assert.AreEqual("1", node.Next.Reference.Tag);
        //    Assert.AreEqual("2", node.Reference.Tag);
        //}


    }
}
