﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RandomLinkedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomLinkedList.Tests
{
    [TestClass]
    public class NodeTests
    {
        [TestMethod]
        public void CloneTest()
        {
            for (var i = 0; i < 100; i++)
            {
                var node = NodeListGenerator.Generate(1000); 
                var clonedNode = Node.DuplicateList(node); 
                while (node.Next != null)
                {
                    Assert.AreEqual(node.Tag, clonedNode.Tag);
                    Assert.AreEqual(node.Reference.Tag, clonedNode.Reference.Tag);
                    Assert.AreNotSame(node, clonedNode);
                    Assert.AreNotSame(node.Reference, clonedNode.Reference);
                    node = node.Next;
                    clonedNode = clonedNode.Next;
                }
                Assert.AreEqual(node.Tag, clonedNode.Tag);
                Assert.AreEqual(node.Reference.Tag, clonedNode.Reference.Tag);
                Assert.AreNotSame(node, clonedNode);
                Assert.AreNotSame(node.Reference, clonedNode.Reference);
            }
        }
    }
}