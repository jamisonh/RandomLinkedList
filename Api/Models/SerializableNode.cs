﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using RandomLinkedList;

namespace Api.Models
{
    [DataContract]
    public class SerializableNode
    {
        [DataMember]
        public string UniqueObjectId { get; set; }
        [DataMember]
        public string Tag { get; set; }
        [DataMember]
        public string ReferenceObjectId { get; set; }
        [DataMember]
        public string ReferenceTag { get; set; }

        public SerializableNode(Node node)
        {
            UniqueObjectId = node.ObjId;
            Tag = node.Tag;
            ReferenceObjectId = node.Reference.ObjId;
            ReferenceTag = node.Reference.Tag;
        }
    }
}