﻿using System.Collections.Generic;
using System.Web.Http;
using Api.Models;
using Newtonsoft.Json.Linq;
using RandomLinkedList;

namespace Api.Controllers
{
    public class LinkedListController : ApiController
    {

        public JObject Get(int length)
        {

            var original = NodeListGenerator.Generate(length);
            var cloned = Node.DuplicateList(original);

            var serializedOriginal = new List<SerializableNode>();
            var serializedCloned = new List<SerializableNode>();

            while (original.Next != null)
            {
                serializedOriginal.Add(new SerializableNode(original));
                serializedCloned.Add(new SerializableNode(cloned));
                original = original.Next;
                cloned = cloned.Next;
            }
            serializedOriginal.Add(new SerializableNode(original));
            serializedCloned.Add(new SerializableNode(cloned));


            dynamic lists = new JObject();
            lists.original = JToken.FromObject(serializedOriginal);
            lists.cloned = JToken.FromObject(serializedCloned);

            return lists;

        }
    }
}
